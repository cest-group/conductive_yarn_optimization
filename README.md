# AI-yarns
Repository for all code and data related to the paper *AI-yarns*.

If you use the provided code or data in your research please cite
- *AI yarns*, M. Ianacchero *et al*. (2024)

## Installation
This repository contains all the scripts used to run BOSS and perfrom the postporcessing plus Pareto front analysis. We recommend that the depedencies 
be installed in a virtual environment. Once you have created and activated environment simply clone this repository and run:

```bash
python3 -m pip install -r requirements.txt
```
## Usage
The repository includes four runnable scripts:
* ``run_bo``: performs one BO iteration to yield a batch of acquistions
* ``run_pareto``: implements the cost model and uses it in combination with the GP models to perform a Pareto front analysis 
* ``plot_models``: plots the GP model (predictive mean and variance).
* ``plot_scan1d``: plots the resistance as a function of the PTSA/pyrrole ratio at fixed pyrrole content.

## Data
The data set consists of initial Sobol points and BO acquistions and distributed as a CSV file ``data/data_master.csv``. The data is organized in a table where samples are stored row-wise with the following columns: 

| Column label | Description | Unit |
| ---      | ---      | ---      |
| ID   | Unique sample identifier   |    |
| Pyrrole | Amount of pyrrole  |  | weight %
| PTSA/Pyrrole | Ratio of PTSA to Pyrrole | |
| R1 | 1st resistance measurement | kOhm  |
| R2 | 2nd resistance measurement | kOhm |
| R3 | 3rd resistance measurement | kOhm |
| Resistance | Average resistance | kOhm |
| acqfn | Acquisition function used to generate sample  |  |
| batch | Batch number of sample |  |
