import matplotlib.pyplot as plt
import numpy as np
from boss.bo.bo_main import BOMain
import pandas as pd


def get_data():
    df_all = pd.read_csv('./data/data_master.csv', index_col='ID')
    inputs = ["PTSA/Pyrrole"]
    outputs = ["R1", "R2", "R3"]
    df = df_all[df_all.index.str.contains("scan")]

    # set the outlier to nan to exclude it in the averaging
    df.loc["scan2_2", "R1"] = np.nan

    X = df[inputs].to_numpy()
    Y = df[outputs].mean(axis=1).to_numpy()
    Y = np.log10(Y)
    return X, Y

# Get data from all batches: 0-3
# User fewer batches to see intermediate models
X, Y = get_data()

noise_scan = np.var(Y[np.isclose(X, 0.25).flatten()])
noise_mle = 0.19166341788643118

def dummy():
    pass

#
bounds = [0, 1.5]
bo = BOMain(
    dummy,
    bounds,
    kernel="rbf",
    noise=noise_mle,
    iterpts=0,
)
res = bo.run(X, Y)

model = res.reconstruct_model(-1)
mu = lambda X: np.squeeze(model.predict(X)[0])
std = lambda X: np.squeeze(np.sqrt(model.predict(X)[1]))
x = np.linspace(bounds[0], bounds[1], 100)[:, None]
y = mu(x)
y_std1 = y - std(x)
y_std2 = y + std(x)
x = np.squeeze(x)


fig, ax = plt.subplots()
ax.set(xlim=[0, 1.5])
ax.plot(x, y, color="tab:blue", label="Prediction")
ax.plot(x, y_std1, color="grey", label="Standard deviation")
ax.plot(x, y_std2, color="grey")
ax.plot(
    X.flatten(),
    Y.flatten(),
    label="Measurement",
    color="tab:red",
    marker="^",
    ls="none",
    clip_on=False,
)
ax.set(xlabel="PTSA/Pyrrole", ylabel=r"$\log_{10}{R}\; (k\Omega)$")
ax.fill_between(x, y_std2, y_std1, color="lightgrey")
ax.legend()
print(f"Noise std determined by replicates: {10**np.sqrt(noise_scan):.3f}")
print(f"Noise std determined by MPE: {10**np.sqrt(noise_mle):.3f}")
plt.subplots_adjust(left=0.17)
# plt.savefig("scan.png", dpi=300)
plt.show()
