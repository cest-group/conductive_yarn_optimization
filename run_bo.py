"""
Performs one iteration of Bayesian optimization using the 
pyrrole-coated yarns data. The result is two new acquisitions
(using a kriging-believer approach) for which to perform experiments.
"""
import numpy as np
import pandas as pd
from boss.bo.bo_main import BOMain
from boss.bo.acq.kb import MultiKrigingBeliever


def get_data(batches=None):
    df = pd.read_csv('./data/data_master.csv', index_col='ID')
    if batches is not None:
        df = df[df["batch"].isin(batches)]

    inputs = ["Pyrrole", "PTSA/Pyrrole"]
    outputs = ["R1", "R2", "R3"]
    X = df.loc[:, inputs].astype(float).to_numpy()
    Y = np.log(df.loc[:, outputs].astype(float).mean(axis=1).to_numpy())
    return X, Y


# Get data from all batches: 0-3
# User fewer batches to see intermediate models
X, Y = get_data(batches=[0, 1, 2, 3])


bounds = [[0.0, 100], [0, 1.5]]

def fdummy():
    raise NotImplementedError


# Run BOSS
# We use batches of 2 acquisitions: 1 with ELCB and 1 with pure exploration
bo = BOMain(
    fdummy,
    bounds,
    kernel="rbf",
    noise=0.2,
    iterpts=0,
)
res = bo.run(X, Y)

acq_manager = MultiKrigingBeliever([bo], {'elcb': 1, 'explore': 1})
X_batch = acq_manager.acquire()

print('New acquisitions:')
for x in X_batch:
    print(f'Pyrrole: {x[0]:.2f}, PTSA/Pyrrole: {x[1]:.2f}')
