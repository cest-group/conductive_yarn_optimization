from __future__ import annotations

from collections.abc import Sequence
from typing import Callable

import matplotlib.pyplot as plt
import numpy as np
import numpy.typing as npt
import pandas as pd
from visualize import Mesher
from boss.bo.bo_main import BOMain


def get_data(batches=None):
    df = pd.read_csv('./data/data_master.csv', index_col='ID')
    if batches is not None:
        df = df[df["batch"].isin(batches)]

    inputs = ["Pyrrole", "PTSA/Pyrrole"]
    outputs = ["R1", "R2", "R3"]
    X = df.loc[:, inputs].astype(float).to_numpy()
    Y = np.log(df.loc[:, outputs].astype(float).mean(axis=1).to_numpy())
    return X, Y


def is_pareto(Y):
    """Identifies Pareto points in the given output data"""
    pareto_mask = np.ones(Y.shape[0], dtype=bool)
    for i, y in enumerate(Y):
        if np.any(np.all(y < Y, axis=1)):
            pareto_mask[i] = False
    return pareto_mask


def calc_pareto(
    funcs: Sequence[Callable],
    bounds: Sequence | npt.NDArray,
    maximize: npt.ArrayLike = True,
    grid_pts: npt.ArrayLike = 30,
) -> tuple[npt.NDArray, npt.NDArray, npt.NDArray]:
    """Performs a Pareto analysis for a given list of functions.

    The feasible points and Pareto front are calculated on a grid and returned
    along with the corresponding x-points in input space corresponding to the
    Pareto front.

    Parameters
    ----------
    funcs : array_like of functions
        Objective functions for which to perform the Pareto analysis, must take a single
        2D array as input and be vectorized.
    bounds : array_like of float
        The input space bounds.
    maximize : array_like of bool
    Array with same length as funcs where maximize[i] is true if the i:th function
        should be maximized and vice versa.
    grid_pts : array_like of int
        The number of points in each dimension used for the grid.

    Returns
    -------
    X_pareto : np.ndarray
        Input space points corresponding to the Pareto front.
    Y_pareto : np.ndarray
        Pareto front, objective values are stored column-wise.
    Y_feasible : np.ndarray
        Feasible points, including the pareto front. Objective values
        are stored column-wise.
    """
    grid_pts = np.atleast_1d(grid_pts).astype(int)
    if len(grid_pts) == 1:
        grid_pts = np.repeat(grid_pts, len(bounds)).astype(int)

    maximize = np.atleast_1d(maximize).astype(int)
    if len(maximize) == 1:
        maximize = np.repeat(maximize, len(funcs)).astype(bool)

    # flip signs on functions that should be minimized
    funcs_new = []
    for i, maxi in enumerate(maximize):
        if not maxi:
            funcs_new.append(lambda X: -1 * funcs[i](X))
        else:
            funcs_new.append(funcs[i])

    Y_feasible = np.zeros((np.prod(grid_pts), len(funcs)))

    mesher = Mesher(bounds, grid_pts=grid_pts)
    for i, f in enumerate(funcs_new):
        Y_feasible[:, i] = np.squeeze(mesher.calc_func(f, coord_shaped=True))

    pareto_mask = np.ones(Y_feasible.shape[0], dtype=bool)
    for i, y in enumerate(Y_feasible):
        if np.any(np.all(y < Y_feasible, axis=1)):
            pareto_mask[i] = False

    Y_pareto = Y_feasible[pareto_mask]
    X_pareto = mesher.coords[pareto_mask]

    # flip signs back
    for i, maxi in enumerate(maximize):
        if not maxi:
            Y_pareto[:, i] *= -1
            Y_feasible[:, i] *= -1

    return X_pareto, Y_pareto, Y_feasible


def materials_cost(X):
    X = np.atleast_2d(X)  # for convenience
    pyrrole_percent = X[:, 0]  # Pyrrole concentration in weight percent
    m_pyrrole = (
        pyrrole_percent / 100
    )  # weight of the pyrrole in grams referred to 1g of yarn
    n_pyrrole = m_pyrrole / 67.091  # moles of pyrrole
    ratio_PTSA_pyrrole = X[:, 1]  # Dopant-to-Pyrrole molar ratio
    n_PTSA = ratio_PTSA_pyrrole * n_pyrrole  # moles of PTSA
    m_PTSA = (
        n_PTSA * 172.2
    ) / 0.9053  # mass of PTSA monohydrate (commercially available)
    # Calculate price in euros based on Pyrrole and PTSA grams required in respect to 1g of substrate
    price = 0.105 * m_pyrrole + 0.012 * m_PTSA
    return price


if __name__ == "__main__":
    # --- fetch data from AMAD and fit a BOSS model to it
    # X, Y = get_data()
    X, Y = get_data(batches=[0, 1, 2, 3])
    bounds = [[0.0, 100], [0, 1.5]]

    def fdummy():
        pass

    bo = BOMain(
        fdummy,
        bounds,
        kernel="rbf",
        noise=0.382,
        iterpts=0,
    )
    res = bo.run(X, Y)

    # function to predict the resistance from the boss model
    model = lambda X: bo.model.predict(X)[0]

    # --- calculate pareto front
    X_pareto, Y_pareto, Y_feasible = calc_pareto(
        [model, materials_cost],
        bounds=bounds,
        maximize=[False, False],
        grid_pts=(25, 25),
    )
    Y_pareto *= np.log10(np.exp(1))
    Y_feasible *= np.log10(np.exp(1))

    # --- plotting
    fig, ax_arr = plt.subplots(1, 2, figsize=(12, 4))
    ax = ax_arr[0]

    sc = ax.scatter(
        Y_feasible[:, 0],
        Y_feasible[:, 1],
        s=30,
        alpha=1.0,
        marker=".",
        color="k",
        label="Feasible point",
        clip_on=False,
    )

    # pareto points
    cmap = 'coolwarm'
    sc = ax.scatter(
        Y_pareto[:, 0],
        Y_pareto[:, 1],
        c=Y_pareto[:, 1],
        s=30,
        cmap=cmap,
        alpha=1.0,
        marker=".",
        # color="tab:red",
        label="Pareto point",
        clip_on=False,
    )
    ax.set_xlabel(r"$\log_{10}{R}\,(k\Omega)$")
    ax.set_ylabel(r"Cost (€$\mathrm{g}^{-1}$)")
    ax.legend()

    # in input space
    ax = ax_arr[1]
    sc = ax.scatter(
        X_pareto[:, 0],
        X_pareto[:, 1],
        c=Y_pareto[:, 1],
        s=40,
        alpha=1.0,
        marker=".",
        cmap=cmap,
        # color="tab:red",
        label="Pareto point",
        clip_on=False,
    )
    cbar = plt.colorbar(sc, ax=ax)
    cbar.set_label(r"Cost (€$\mathrm{g}^{-1}$)", rotation=270, labelpad=13)
    ax.set_xlabel(r"Pyrrole (\%)")
    ax.set_ylabel("PTSA/Pyrrole")
    ax.set(xlim=bounds[0], ylim=bounds[1])

    # plt.tight_layout()
    plt.subplots_adjust(wspace=0.35)
    # plt.savefig("pareto.png", bbox_inches="tight", dpi=800)
    plt.show()
