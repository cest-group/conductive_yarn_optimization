"""
This scripts fits GP models the the pyrrole-coated yarns data
using the BOSS code and visualizes them.
"""
import matplotlib.pyplot as plt
import numpy as np
from boss.bo.bo_main import BOMain
import pandas as pd
from visualize import grid_calc


def get_data(batches=None):
    df = pd.read_csv('./data/data_master.csv', index_col='ID')
    if batches is not None:
        df = df[df["batch"].isin(batches)]

    inputs = ["Pyrrole", "PTSA/Pyrrole"]
    outputs = ["R1", "R2", "R3"]
    X = df.loc[:, inputs].astype(float).to_numpy()
    Y = np.log(df.loc[:, outputs].astype(float).mean(axis=1).to_numpy())
    return X, Y


# Get data from all batches: 0-3
# User fewer batches to see intermediate models
X, Y = get_data(batches=[0, 1, 2, 3])

bounds = [[0.0, 100], [0, 1.5]]

def fdummy():
    raise ValueError("Dont call")


bo = BOMain(
    fdummy,
    bounds,
    kernel="rbf",
    noise=0.382,
    iterpts=0,
)
bo.init_run(X, Y)

res = bo.results

acqfn = res.reconstruct_acq_func(-1)
model = lambda X: bo.model.predict(X)[0]
model_std = lambda X: np.sqrt(bo.model.predict(X)[1])

fig, axr = plt.subplots(1, 2, sharey=True, figsize=(12, 4))
labels = [
    r"$\log{R}\; (k\Omega)$",
    r"$\mathrm{std}\left[log{R}\right]\; (k\Omega)$",
]

n_sobol = 5
for i, target in enumerate([model, model_std]):
    ax = axr[i]
    label = labels[i]

    grid, Z = grid_calc(target, bounds)
    Z = Z * np.log10(np.exp(1))

    conts = ax.contourf(*grid, Z, levels=20, cmap="coolwarm")
    cbar = plt.colorbar(conts, ax=ax, label=label)

    # sobol points
    X_acqs = res["X"]
    ax.scatter(
        X_acqs[:n_sobol, 0],
        X_acqs[:n_sobol, 1],
        s=30,
        marker="o",
        color='yellow',
        label="Sobol",
        facecolor="none",
        clip_on=False,
        linewidth=2,
    )

    # acquisitions
    if len(X_acqs) > n_sobol:
        ax.scatter(
            X_acqs[n_sobol:, 0],
            X_acqs[5:, 1],
            s=30,
            marker="s",
            color='tab:green',
            label="Acquisition",
            facecolor="none",
            clip_on=False,
            linewidth=2,
        )

    # minimum prediction
    if i == 0:
        x_glmin = res.select("x_glmin", -1)
        ax.scatter(
            x_glmin[0], x_glmin[1], s=50, marker="*", color="k", label="Minimum", zorder=10, clip_on=False
        )
        ax.legend()

    ax.set_xlabel(r"Pyrrole (\%weight)")
    ax.set_ylabel("PTSA/Pyrrole")

plt.show()
